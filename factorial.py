import sys
sys.path.append('..')
import func.valids as vd

def factorial(i):
    _ = vd.validate_num(i)
    if i == 1:
        return 1
    else:
        return i * factorial(i-1)

