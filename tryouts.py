import sys
sys.path.append('..')
import func.fabonacci as fb, func.factorial as fc

print(fb.fab(6))
print(fc.factorial(6))

while(1):
    try:
        print('Tyring')
        N = float(input('Please enter zero or a positive real number:     '))
        print(fb.fab(N))
        print(fc.factorial(N))
        break
    except ValueError as ve:
        print(ve)
    except:
        print('Program did not run')

