import sys
sys.path.append('..')
import func.valids as vd


def fab(i):
    _ = vd.validate_num(i)
    if i == 1:
        return 1
    if i == 0:
        return 0
    else:
        return fab(i-1)+fab(i-2)

