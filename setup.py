import setuptools

with open('README.md', 'r') as fh:
    long_description = fh.read()


setuptools.setup(
    name='trying_algos',
    version='0.0.1',
    author='AamirAyub',
    author_email='aayub02@gmail.com',
    description='This package is for testing purposes only',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://github.com/aayub02/leakdetection/',
    packages=setuptools.find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
    ],
    python_requires='>=3.6'
)