
def validate_num(value=None):
    if value is not None:
        if not isinstance(value, (int, float)) or value<0:
            raise ValueError('Input must be zero or a positive real number')
        else:
            return value
    else:
        raise ValueError('Input cannot be empty')
